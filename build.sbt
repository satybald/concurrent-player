name := "Scala Concurency Primitives"

version := "1.0-SNAPSHOT"

resolvers ++= Seq(
  "Sonatype OSS Snapshots" at "https://oss.sonatype.org/content/repositories/snapshots",
  "Sonatype OSS Releases" at "https://oss.sonatype.org/content/repositories/releases",
  "Typesafe Repository" at "http://repo.typesafe.com/typesafe/releases/"
)

fork := false

libraryDependencies += "org.scala-stm" %% "scala-stm" % "0.7"

libraryDependencies += "com.storm-enroute" %% "scalameter-core" % "0.6"

libraryDependencies += "com.typesafe.akka" %% "akka-transactor" % "2.1.2"

libraryDependencies += "com.typesafe.akka" %% "akka-testkit" % "2.1.2"

libraryDependencies += "com.typesafe.akka" %% "akka-agent" % "2.1.2"

libraryDependencies += "com.typesafe.akka" %% "akka-actor" % "2.1.2"

libraryDependencies += "org.scalatest" % "scalatest_2.10" % "2.0" % "test"


val scalaMeterFramework = new TestFramework("org.scalameter.ScalaMeterFramework")

testFrameworks in ThisBuild += scalaMeterFramework

testOptions in ThisBuild += Tests.Argument(scalaMeterFramework, "-silent")

parallelExecution in Test := false