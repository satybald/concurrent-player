package com.satybald.player

import java.util.concurrent.locks.{Lock, ReentrantReadWriteLock}

object ReentrantReadWritePlayer extends Player {
  private val rwLock = new ReentrantReadWriteLock()
  private val rLock = rwLock.readLock()
  private val wLock = rwLock.writeLock()

  private var points: Long = 0L;
  private var kudos: Long = 0L;


  override def update(point: Long, kudo: Long): Unit = {
    wLock.lock();
    try {
      points = point
      kudos = kudo
    } finally {
      wLock.unlock();
    }
  }

  override def getPoints(): Long = {
    rLock.lock();
    try {
      points
    } finally {
      rLock.unlock();
    }
  }

  override def getKudos(): Long = {
    rLock.lock();
    try {
      kudos
    } finally {
      rLock.unlock();
    }
  }

  override def getScores(): (Long, Long) = {
    rLock.lock();
    try {
      (points, kudos)
    } finally {
      rLock.unlock();
    }

  }
}