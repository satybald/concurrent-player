package com.satybald.player


import scala.concurrent.stm.Ref
import akka.actor.Actor
import akka.transactor.Coordinated


class ActorPlayerSTM extends Actor {
  var kudos: Ref[Long] = Ref(0L)
  var points: Ref[Long] = Ref(0L)

  def receive = {
    case msg@KudosScore => sender ! kudos.single.get
    case value@PointsScore => sender ! points.single.get
    case coordinated@Coordinated(scores: UpdateScores) => coordinated atomic { implicit t =>
      kudos.single.set(scores.kudo)
      points.single.set(scores.point)
    }
  }
}