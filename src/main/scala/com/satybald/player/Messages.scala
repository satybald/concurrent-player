package com.satybald.player

/**
 * Created by sayat on 1/5/15.
 */

sealed trait Messages

case object KudosScore extends Messages

case object PointsScore extends Messages

case class UpdateScores(point: Long, kudo: Long) extends Messages
