package com.satybald.player

/**
 * Created by sayat on 1/2/15.
 */
object SyncronizedLockPlayer extends Player {
  private var kudos: Long = 0L
  private var points: Long = 0L
  private val lock = new Object()


  def update(point: Long, kudo: Long): Unit = {
    lock.synchronized {
      kudos = kudo
      points = point
    }
  }

  def getPoints(): Long = lock.synchronized {
    points
  }

  def getKudos(): Long = lock.synchronized {
    kudos
  }

  override def getScores(): (Long, Long) = lock.synchronized {
    (points, kudos)
  }


}
