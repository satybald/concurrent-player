package com.satybald.player

object SyncronizedPlayer extends Player {
  private var kudos: Long = 0L
  private var points: Long = 0L


  def update(point: Long, kudo: Long): Unit = this.synchronized {
    kudos = kudo
    points = point
  }

  def getPoints(): Long = this.synchronized {
    points
  }

  def getKudos(): Long = this.synchronized {
    kudos
  }

  override def getScores(): (Long, Long) = this.synchronized {
    (points, kudos)
  }
}