package com.satybald.player

import akka.agent.Agent
import akka.actor.ActorSystem

/**
 * Created by sayat on 1/2/15.
 */

object AkkaPlayerAgent extends Player {
  implicit val system = ActorSystem("playerApp")
  val kudoAgent = Agent(0L)
  val pointAgent = Agent(0L)

  override def update(point: Long, kudo: Long): Unit = {
    pointAgent send point
    kudoAgent send kudo
  }

  override def getKudos(): Long = kudoAgent get()

  override def getPoints(): Long = pointAgent get()
}
