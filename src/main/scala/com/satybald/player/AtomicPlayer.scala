package com.satybald.player

import java.util.concurrent.atomic.AtomicLong


object AtomicPlayer extends Player {
  
  private val points = new AtomicLong(0L)
  private val kudos = new AtomicLong(0L)
  private val lock = new Object()

  override def update(point: Long, kudo: Long) = lock.synchronized {
    points.set(point)
    kudos.set(kudo)
  }

  override def getPoints(): Long = {
    points.get()
  }

  override def getKudos(): Long = {
    kudos.get()
  }

  override def getScores(): (Long, Long) = {
    (points.get(), kudos.get())
  }

}