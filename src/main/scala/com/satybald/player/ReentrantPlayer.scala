package com.satybald.player

import java.util.concurrent.atomic
import java.util.concurrent.locks.{Lock, ReentrantLock}
;


object ReentrantPlayer extends Player {
  private val lock: Lock = new ReentrantLock()
  private var points: Long = 0L
  private var kudos: Long = 0L


  def update(point: Long, kudo: Long): Unit = {
    lock.lock();
    try {
      points = point
      kudos = kudo
    } finally {
      lock.unlock();
    }
  }

  def getPoints(): Long = {
    lock.lock();
    try {
      points
    } finally {
      lock.unlock();
    }
  }

  def getKudos(): Long = {
    lock.lock();
    try {
      kudos
    } finally {
      lock.unlock();
    }
  }

  override def getScores(): (Long, Long) = {
    lock.lock();
    try {
      (points, kudos)
    } finally {
      lock.unlock();
    }

  }


}