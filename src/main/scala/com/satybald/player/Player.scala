package com.satybald.player

trait Player {

  def update(point: Long, kudo: Long): Unit;

  def getPoints(): Long;

  def getKudos(): Long;

  def getScores(): (Long, Long) = ???
}