package com.satybald.player

object VolatilePlayer extends Player {
  @volatile private var points: Long = 0L
  @volatile private var kudos: Long = 0L
  //	case class PlayerScore(@volatile var points: Long, @volatile var kudos: Long)


  def update(point: Long, kudo: Long) = this.synchronized {
    points = point
    kudos = kudo

  }

  def getPoints(): Long = {
    points
  }

  def getKudos(): Long = {
    kudos
  }

  override def getScores(): (Long, Long) = {
    (points, kudos)
  }
}