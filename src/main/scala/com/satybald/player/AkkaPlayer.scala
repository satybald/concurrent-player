package com.satybald.player

import akka.actor._

import scala.concurrent.Future
import scala.concurrent.duration._
import akka.util.Timeout
import akka.pattern.ask
import scala.concurrent.ExecutionContext.Implicits.global

class AkkaPlayer extends Actor with ActorLogging {
  var kudos: Long = 0L
  var points: Long = 0L


  override def receive = {
    case kudo@KudosScore => {
      sender ! kudos
    }
    case point@PointsScore => {
      sender ! points
    }
    case UpdateScores(point, kudo) => {
      kudos = kudo
      points = point
    }
  }
}

object AkkaPlayerObj extends App {
  val system = ActorSystem("AkkaPlayerSystem")
  val actor: ActorRef = system.actorOf(Props[AkkaPlayer], "AkkaPlayer")
  implicit val timeout = Timeout(5 seconds)

  println(actor ! KudosScore)
  actor ! PointsScore

  actor ! UpdateScores(10, 10)

  val scr = actor ask KudosScore
  scr.onSuccess {
    case x => println("ask: " + x)
  }
  actor ! PointsScore
  println(actor ! KudosScore)

  system.shutdown()
}
