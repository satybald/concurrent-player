package com.satybald.player

import scala.concurrent.stm._

object PlayerSTM extends Player {
  var kudos: Ref[Long] = Ref(0L)
  var points: Ref[Long] = Ref(0L)

  def update(point: Long, kudo: Long): Unit = atomic { implicit tnx =>
    kudos() = kudo
    points() = point
  }

  def getPoints(): Long = atomic { implicit tnx =>
    points()
  }

  def getKudos(): Long = atomic { implicit tnx =>
    kudos()
  }

  override def getScores(): (Long, Long) = atomic { implicit tnx =>
    (points(), kudos())
  }

}