package com.satybald.bench

import scala.concurrent.Await
import scala.concurrent.duration._
import akka.util.Timeout
import akka.actor.{Props, ActorRef, ActorSystem}
import akka.pattern.ask

import org.scalameter._

import com.satybald.player._

object Performance extends App{

  class CPlayerWorker(player: Player, rRatio: Int, wRatio: Int) extends Thread{

    override def run(): Unit ={
      for( i <- 1 to wRatio){
        player.getKudos()
        player.update(i, i + 1)
      }
      for(j <- 1 to rRatio){
        player.getPoints()
      }

    }
  }

  /**
   * Thread Wrapper for measuring Akka Performance
   * @param aktor
   * @param rRatio
   * @param wRatio
   */
  class CAkkaWorker(aktor: ActorRef, rRatio: Int, wRatio: Int) extends Thread{

    override def run(): Unit ={
      for( i <- 1 to wRatio){
        val scoreKudo = actor ask KudosScore
        val kScore = Await.result(scoreKudo, timeout.duration).asInstanceOf[Long]
        actor ! UpdateScores(i, i+1)
      }
      for(j <- 1 to rRatio){
        val scorePoint = actor ask PointsScore
        val pScore = Await.result(scorePoint, timeout.duration).asInstanceOf[Long]
      }

    }
  }

  class Wrapper(player: Player) {
    val runTime = conf measure {

      val threads = for (j <- 0 until threadCount) yield new CPlayerWorker(player, readRatio, writeRatio)
      for (t <- threads) t.start()
      for (t <- threads) t.join()

    }

    println(s"${player.getClass},$runTime,$threadCount,$readRatio,$writeRatio")

  }


  class AkkaWrapper(aktor: ActorRef) {
    val runTime = conf measure {

      val threads = for (j <- 0 until threadCount) yield new CAkkaWorker(actor, readRatio, writeRatio)
      for (t <- threads) t.start()
      for (t <- threads) t.join()

    }

    println(s"${aktor.path},$runTime,$threadCount,$readRatio,$writeRatio")

  }

  val system = ActorSystem("AkkaSystem")
  val actor: ActorRef = system.actorOf(Props[ AkkaPlayer], "AkkaPlayer")
  val actorSTM: ActorRef = system.actorOf(Props[ActorPlayerSTM], "AkkaPlayerSTM")
  implicit val timeout = Timeout(5 seconds)

  val conf = config(
    Key.exec.minWarmupRuns -> 2,
    Key.exec.maxWarmupRuns -> 5,
    Key.exec.benchRuns -> 2,
    Key.verbose -> false
  ) withWarmer{new Warmer.Default}
    withMeasurer {new Measurer.IgnoringGC}

  var threadCount = 0
  var readRatio = 0
  var writeRatio = 0
  println(s"Class,time,threadCount,readRatio,writeRatio")
  for (trCount <- List(1, 100, 10000)){
    threadCount = trCount
    for( (r: Int,w: Int) <- List((6, 6), (2, 10), (10, 2))){
      readRatio = r
      writeRatio = w
      new Wrapper(SyncronizedPlayer)
      new Wrapper(SyncronizedLockPlayer)
      new Wrapper(PlayerSTM)

      new Wrapper(AtomicPlayer)
      new Wrapper(ReentrantPlayer)
      new Wrapper(ReentrantReadWritePlayer)
      new Wrapper(VolatilePlayer)
      new Wrapper(AkkaPlayerAgent)

      new AkkaWrapper(actor)
      new AkkaWrapper(actorSTM)
    }

  }

  system.shutdown()

}