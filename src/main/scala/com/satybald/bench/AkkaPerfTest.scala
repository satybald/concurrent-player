package com.satybald.bench

import akka.actor.{ActorRef, ActorSystem, Props}
import akka.util.Timeout
import scala.concurrent.Await
import scala.concurrent.duration._
import com.satybald.player._
import org.scalameter._
import akka.pattern.ask
import akka.dispatch._
import scala.concurrent.ExecutionContext.Implicits.global

/**
 * Created by sayat on 1/5/15.
 */
object AkkaPerfTest extends App {

  val conf = config(
    Key.exec.minWarmupRuns -> 20,
    Key.exec.maxWarmupRuns -> 40,
    Key.exec.benchRuns -> 30,
    Key.verbose -> false
  ) withWarmer{new Warmer.Default}
  withMeasurer {new Measurer.IgnoringGC}

  class PerfWrapper(actor : ActorRef){

    val runTime = conf measure {
      for (i <- 1 to writeRatio) {
        actor ! UpdateScores(i, i)
      }

      for (i <- 1 to readRatio) {
        val scoreKudo = actor ask KudosScore
        val kScore = Await.result(scoreKudo, timeout.duration).asInstanceOf[Long]

        val scorePoint = actor ask PointsScore
        val pScore = Await.result(scorePoint, timeout.duration).asInstanceOf[Long]
      }
    }
    println(s"${actor.path},$runTime,$threadCount,$readRatio,$writeRatio")
  }


  val system = ActorSystem("AkkaPlayerSystem")
  val actor: ActorRef = system.actorOf(Props[ AkkaPlayer], "AkkaPlayer")
  val actorSTM: ActorRef = system.actorOf(Props[ActorPlayerSTM], "AkkaPlayerSTM")
  implicit val timeout = Timeout(5 seconds)

  var threadCount = 0
  var readRatio = 0
  var writeRatio = 0

  println(s"class,time,threadCount,readRatio,writeRatio")

  for (trCount <- List(1, 10, 100, 10000)){
      threadCount = trCount

      for( (r: Int,w: Int) <- List((6, 6), (1, 10), (10, 1))) {
        readRatio = r
        writeRatio = w
        new PerfWrapper(actor)
        new PerfWrapper(actorSTM)
      }
    }

  system.shutdown()

}
