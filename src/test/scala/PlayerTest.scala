/**
 * Created by sayat on 12/30/14.
 */

import com.satybald.player._
import org.scalatest._
import org.scalatest.prop._


class PlayerTest extends WordSpec with Matchers {

  def atomicallyUpdate(player: Player): Unit = {
    class Update(point: Long, kudo: Long) extends Thread {
      override def run(): Unit = {
        player.update(point, kudo)
        val score = player.getScores()
        assert(score._1 == score._2)
      }
    }

    val T = for (i <- 600000L to 1000000L)
      yield new Update(i, i)

    T.foreach(_.start())
    T.foreach(_.join())
    assert(player.getKudos() == player.getPoints())

  }

  "A player" should {

    "be SyncronizedPlayer atomically updated" in {
      val ct = SyncronizedPlayer
      atomicallyUpdate(ct)
    }

    "be VolatilePlayer atomically updated" in {
      val ct = VolatilePlayer
      atomicallyUpdate(ct)
    }

    "be AtomicPlayer atomically updated" in {
      val ct = AtomicPlayer
      atomicallyUpdate(ct)
    }

    "be ReentrantPlayer atomically updated" in {
      val ct = ReentrantPlayer
      atomicallyUpdate(ct)
    }

    "be ReentrantReadWritePlayer atomically updated" in {
      val ct = ReentrantReadWritePlayer
      atomicallyUpdate(ct)
    }


    "be PlayerSTM atomically updated" in {
      val ct = PlayerSTM
      atomicallyUpdate(ct)
    }

    def concurrentPlayerThreads(player: Player) = {

      class Updater(point: Int, kudo: Int) extends Thread {
        override def run() {
          player.update(point, kudo)
          player.getPoints match {
            case score if score % 2 == 0 => assert(player.getKudos() % 2 == 0)
            case score if score % 2 != 0 => assert(player.getKudos() % 2 != 0)
          }
        }
      }

      val T1 = for (i <- 0 to 100 if i % 2 == 0;
                    j <- 0 to 10 if j % 2 == 0)
      yield new Updater(i, j)

      val T2 = for (i <- 1 to 100 if i % 2 != 0;
                    j <- 1 to 10 if j % 2 != 0) yield new Updater(i, j)

      T2.foreach(_.start())
      T1.foreach(_.start())


      T1.foreach(_.join())
      T2.foreach(_.join())

      assert(player.getPoints() == 100)
      assert(player.getKudos() == 10)
    }

    "volatile player concurently modifing scores" in {
      val ct = VolatilePlayer
      concurrentPlayerThreads(ct)
    }

    "syncronized player concurently modifing scores" in {
      val ct = SyncronizedPlayer
      concurrentPlayerThreads(ct)
    }


    "reentrant player concurrently modyfing scores" in {
      val ct = ReentrantPlayer
      concurrentPlayerThreads(ct)
    }

    "STM concurrently modyfing scores" in {
      val ct = PlayerSTM
      concurrentPlayerThreads(ct)
    }

    "Atomics concurrently modyfing scores" in {
      val ct = AtomicPlayer
      concurrentPlayerThreads(ct)
    }


  }
}
